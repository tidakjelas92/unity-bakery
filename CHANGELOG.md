# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [1.0.0] - 2023-06-21

### Added

- Batch baking according to selected jobs
- Bake lightmap and bake occlusion

[unreleased]: https://gitlab.com/tidakjelas92/unity-bakery/-/compare/v0.1.0...main
[1.0.0]: https://gitlab.com/tidakjelas92/unity-bakery/-/tags/v1.0.0
