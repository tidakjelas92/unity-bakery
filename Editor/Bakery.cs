// Source:
// Thread post from @dgoyette
// https://forum.unity.com/threads/baking-lighting-via-command-line-script.751259/

using System.IO;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace TidakJelas.Bakery
{
    public class Bakery : EditorWindow
    {
        private struct BakeJob
        {
            public string scenePath;
            public bool lightmap;
            public bool occlusion;
        }

        private BakeJob[] _jobs = new BakeJob[] { };
        private int _currentScene;
        private int _completed;
        private int _total;

        private readonly Color _separatorColor = new Color(0.3f, 0.3f, 0.3f);

        [MenuItem("Tools/TidakJelas/Bakery")]
        private static void Initialize()
        {
            var window = (Bakery)GetWindow(typeof(Bakery));
            window.titleContent = new GUIContent("Bakery");
            window.position = new Rect(0, 0, 300, 100);
            window.Show();
        }

        private void OnGUI()
        {
            if (Lightmapping.isRunning)
            {
                EditorGUI.ProgressBar(
                    new Rect(5, 5, position.width - 10, 20),
                    Lightmapping.buildProgress,
                    "Baking lightmap..."
                );
                EditorGUI.ProgressBar(
                    new Rect(5, 30, position.width - 10, 20),
                    (float)_completed / (float)_total,
                    $"Completed: {_completed}/{_total}"
                );

                if (GUI.Button(new Rect(5, 55, position.width - 10, 20), "Cancel"))
                {
                    CancelBake();
                    EditorGUIUtility.ExitGUI();
                }

                return;
            }

            BuildJobsGUI();

            if (GUILayout.Button("Load Scenes Data"))
            {
                LoadScenesData();
                EditorGUIUtility.ExitGUI();
            }

            if (_jobs.Length > 0)
            {
                DrawHorizontalSeparator(_separatorColor);

                if (GUILayout.Button("Clear Selected Baked Data"))
                {
                    ClearSelectedBakedData();
                    EditorGUIUtility.ExitGUI();
                }

                if (GUILayout.Button("Bake Selected"))
                {
                    BakeSelected();
                    EditorGUIUtility.ExitGUI();
                }
            }
        }

        private void BuildJobsGUI()
        {
            if (_jobs.Length <= 0)
                return;

            EditorGUILayout.BeginHorizontal();

            var buttonStyle = new GUIStyle(GUI.skin.button) { fixedWidth = 80.0f };
            if (GUILayout.Button("Select All", buttonStyle))
            {
                SelectAll();
                EditorGUIUtility.ExitGUI();
            }
            if (GUILayout.Button("Deselect All", buttonStyle))
            {
                DeselectAll();
                EditorGUIUtility.ExitGUI();
            }

            EditorGUILayout.EndHorizontal();

            EditorGUILayout.BeginHorizontal();

            var columnWidth = 70.0f;
            var toggleStyle = new GUIStyle(GUI.skin.toggle) { alignment = TextAnchor.MiddleCenter };
            var columnLabelStyle = new GUIStyle(GUI.skin.label)
            {
                alignment = TextAnchor.MiddleCenter,
                fontStyle = FontStyle.Bold
            };

            EditorGUILayout.BeginVertical(GUILayout.MaxWidth(columnWidth));
            EditorGUILayout.LabelField(
                "Lightmap",
                columnLabelStyle,
                GUILayout.MaxWidth(columnWidth)
            );
            GUILayout.Space(3f);
            for (var i = 0; i < _jobs.Length; i++)
            {
                GUILayout.BeginHorizontal();
                GUILayout.FlexibleSpace();
                _jobs[i].lightmap = GUILayout.Toggle(_jobs[i].lightmap, string.Empty);
                GUILayout.FlexibleSpace();
                GUILayout.EndHorizontal();
                GUILayout.Space(2f);
            }
            EditorGUILayout.EndVertical();
            DrawVerticalSeparator(_separatorColor);

            EditorGUILayout.BeginVertical(GUILayout.MaxWidth(columnWidth));
            EditorGUILayout.LabelField(
                "Occlusion",
                columnLabelStyle,
                GUILayout.MaxWidth(columnWidth)
            );
            GUILayout.Space(3f);
            for (var i = 0; i < _jobs.Length; i++)
            {
                GUILayout.BeginHorizontal();
                GUILayout.FlexibleSpace();
                _jobs[i].occlusion = GUILayout.Toggle(_jobs[i].occlusion, string.Empty);
                GUILayout.FlexibleSpace();
                GUILayout.EndHorizontal();
                GUILayout.Space(2f);
            }
            EditorGUILayout.EndVertical();
            DrawVerticalSeparator(_separatorColor);

            EditorGUILayout.BeginVertical();
            EditorGUILayout.LabelField(
                "Scene",
                new GUIStyle(GUI.skin.label) { fontStyle = FontStyle.Bold }
            );
            for (var i = 0; i < _jobs.Length; i++)
                GUILayout.Label(_jobs[i].scenePath);
            EditorGUILayout.EndVertical();

            EditorGUILayout.EndHorizontal();

            DrawHorizontalSeparator(_separatorColor);
        }

        private void BakeSelected()
        {
            Debug.Log("Bake selected.");

            _currentScene = 0;
            _completed = 0;
            _total = GetSelectedScenes();
            Lightmapping.bakeCompleted += SaveAndContinueBaking;

            EditorSceneManager.SaveOpenScenes();
            BakeNextScene();
        }

        private void CancelBake()
        {
            Lightmapping.Cancel();
            Debug.Log("Baking cancelled.", this);
        }

        private void ClearSelectedBakedData()
        {
            if (GetSelectedScenes() <= 0)
                return;

            var currentScene = EditorSceneManager.GetActiveScene().path;

            for (var i = 0; i < _jobs.Length; i++)
            {
                var job = _jobs[i];

                EditorSceneManager.OpenScene(job.scenePath);
                if (job.lightmap)
                    Lightmapping.ClearLightingDataAsset();

                if (job.occlusion)
                    StaticOcclusionCulling.Clear();

                if (!EditorSceneManager.SaveOpenScenes())
                    Debug.LogError($"{job.scenePath} was not successfuly saved.", this);

                if (job.lightmap && job.occlusion)
                {
                    var bakedDataDir = Path.Join(
                        Directory.GetParent(job.scenePath).ToString(),
                        Path.GetFileNameWithoutExtension(job.scenePath)
                    );
                    FileUtil.DeleteFileOrDirectory(bakedDataDir);
                    FileUtil.DeleteFileOrDirectory(bakedDataDir + ".meta");
                }
            }

            EditorSceneManager.OpenScene(currentScene);
            AssetDatabase.Refresh();

            Debug.Log("Cleared all baked data of selected scenes.", this);
        }

        private int GetSelectedScenes()
        {
            var result = 0;
            for (var i = 0; i < _jobs.Length; i++)
            {
                if (_jobs[i].lightmap || _jobs[i].occlusion)
                    result += 1;
            }

            return result;
        }

        private void BakeNextScene()
        {
            if (_currentScene >= _jobs.Length)
            {
                EndBake();
                return;
            }

            var job = _jobs[_currentScene];
            if (!job.lightmap)
            {
                Debug.Log($"Skipping scene {job.scenePath} because it is not included", this);
                _currentScene += 1;
                BakeNextScene();
                return;
            }

            EditorSceneManager.OpenScene(job.scenePath);
            if (job.occlusion)
            {
                Debug.Log($"Baking occlusion for {job.scenePath}");
                StaticOcclusionCulling.Compute();
            }

            Debug.Log($"Baking lighting for {job.scenePath}", this);
            if (!Lightmapping.GetLightingSettingsForScene(SceneManager.GetActiveScene()))
                Debug.LogWarning(
                    $"No lighting settings has been assigned to the scene: {job.scenePath}. Lightmap bake will proceed with default settings.",
                    this
                );

            if (!Lightmapping.BakeAsync())
                SaveAndContinueBaking();
        }

        private void EndBake()
        {
            Debug.Log("Bake completed.", this);
            Lightmapping.bakeCompleted -= SaveAndContinueBaking;
        }

        private void SaveAndContinueBaking()
        {
            EditorSceneManager.SaveOpenScenes();
            _currentScene += 1;
            _completed += 1;
            BakeNextScene();
        }

        private void DeselectAll()
        {
            var jobs = _jobs;
            for (var i = 0; i < jobs.Length; i++)
            {
                jobs[i].lightmap = false;
                jobs[i].occlusion = false;
            }

            _jobs = jobs;
        }

        private void SelectAll()
        {
            var jobs = _jobs;
            for (var i = 0; i < jobs.Length; i++)
            {
                jobs[i].lightmap = true;
                jobs[i].occlusion = true;
            }

            _jobs = jobs;
        }

        private void LoadScenesData()
        {
            var sceneCount = SceneManager.sceneCountInBuildSettings;
            _jobs = new BakeJob[sceneCount];

            for (var i = 0; i < sceneCount; i++)
                _jobs[i] = new BakeJob()
                {
                    lightmap = true,
                    occlusion = true,
                    scenePath = SceneUtility.GetScenePathByBuildIndex(i)
                };
        }

        // Adapted from @alexanderameye
        private void DrawVerticalSeparator(Color color, int thickness = 1, int padding = 10)
        {
            var rect = EditorGUILayout.GetControlRect(GUILayout.Width(padding + thickness));
            rect.width = thickness;
            rect.x += padding / 2;
            EditorGUI.DrawRect(rect, color);
        }

        private void DrawHorizontalSeparator(Color color, int thickness = 1, int padding = 10)
        {
            var rect = EditorGUILayout.GetControlRect(GUILayout.Height(padding + thickness));
            rect.height = thickness;
            rect.y += padding / 2;
            EditorGUI.DrawRect(rect, color);
        }
    }
}
